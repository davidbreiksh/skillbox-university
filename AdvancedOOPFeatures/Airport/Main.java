import com.skillbox.airport.Airport;
import com.skillbox.airport.Flight;

import java.util.Date;

public class Main {
    public static void main(String[] args) {

        Airport airport = Airport.getInstance();

        Date date = new Date();

        System.out.println(date);

        airport.getTerminals().forEach(t -> t.getFlights().stream()
                .filter(f-> f.getType() == Flight.Type.DEPARTURE)
                .filter(f -> f.getDate().getTime() - System.currentTimeMillis() <= 7200000)
                .filter(f -> f.getDate().getTime() - System.currentTimeMillis() > 0).forEach(System.out::println));
    }
}