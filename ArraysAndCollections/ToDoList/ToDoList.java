import java.util.ArrayList;
import java.util.Scanner;

public class ToDoList {

    public static void main(String[] args) {

        String command;

        ArrayList<String> toDoList = new ArrayList<>() {{
            add("Wake up");
            add("Breakfast");
            add("Check emails");
            add("Go to work");
        }};
        System.out.println("This is your 'TO DO' list :");
        for (String item : toDoList) {
            System.out.println(item);
        }
        System.out.println("");

        System.out.println("what do you want to do ? ADD - adding case , DELETE - deleting case , EDIT - change case");
        Scanner scanner = new Scanner(System.in);
        command = scanner.nextLine();
        if (command.length() <= 0) {
            System.out.println("Error");
        }

        if (command.equals("ADD")) {
            System.out.println("Write new case");
            command = scanner.nextLine();
            if (command.length() <= 0) {
                System.out.println("Error");
                return;
            }
            System.out.println("Put number of case " + " " + toDoList.size());
            int addToIndex = scanner.nextInt();
            if (addToIndex > toDoList.size()) {
                System.out.println("Error");
                return;
            }
            toDoList.add(addToIndex, command);
            for (String item1 : toDoList) {
                System.out.println(item1);
            }
        }
        if (command.equals("DELETE")) {
            System.out.println("Put number of case you want to delete from 0 to  " + " " + toDoList.size());
            int indexOfList = scanner.nextInt();
            if (indexOfList > toDoList.size()) {
                System.out.println("Error");
                return;
            }
            toDoList.remove(indexOfList);
            for (String item2 : toDoList) {
                System.out.println(item2);
            }
        }
        if (command.equals("EDIT")) {
            System.out.println("Write new case");
            String newList = scanner.nextLine();
            if (newList.length() <= 0) {
                System.out.println("Error");
                return;
            }
            System.out.println("Put number of case you want to change from 0 to  " + " " + toDoList.size());
            int editIndex = scanner.nextInt();
            if (editIndex > toDoList.size()) {
                System.out.println("Error");
                return;
            }
            toDoList.set(editIndex, newList);
            for (String item3 : toDoList) {
                System.out.println(item3);
            }
        }
    }
}