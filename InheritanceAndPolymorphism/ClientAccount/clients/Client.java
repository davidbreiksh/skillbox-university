public abstract class Client {

    public double clientAccount = 10000.0;

    public abstract double checkClientsAccount();

    public double depositMoney(double deposit) {
        System.out.println("Your deposit amount is : " + deposit + " EUR");
        return clientAccount = clientAccount + deposit;
    }

    public double withdrawMoney(double withdraw) {
        if (withdraw > clientAccount) {
            System.out.println("Not enough money");
            return clientAccount;
        }
        return clientAccount = clientAccount - withdraw;
    }
}