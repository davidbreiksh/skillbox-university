public class LegalClient extends Client {

    private final double COMMISSION_PERCENTAGE = 0.01;

    @Override
    public double checkClientsAccount() {
        System.out.println("Your balance is : " + clientAccount + " EUR");
        return clientAccount;
    }

    @Override
    public double withdrawMoney(double withdraw) {
        double commission = withdraw * COMMISSION_PERCENTAGE;
        withdraw = commission + withdraw;
        System.out.println("Commission is : " + commission + " EUR");
        System.out.println("Amount with commission " + withdraw + " EUR");
        return super.withdrawMoney(withdraw);
    }
}