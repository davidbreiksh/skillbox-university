package core;

public class Car
{
    public String number;
    public int height;
    public double weight;
    public boolean hasVehicle;
    public boolean isSpecial;

    public String toString()
    {
        String special = isSpecial ? "Transport " : "";
        return "\n=========================================\n" +
            special + " Car number plate " + number +
            ":\n\theight: " + height + " mm\n\tMass: " + weight + " kg";
    }
}