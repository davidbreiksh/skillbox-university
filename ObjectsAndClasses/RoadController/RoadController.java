import core.*;
import core.Camera;

import java.util.Scanner;

public class RoadController
{
    private static double passengerCarMaxWeight = 3500.0;
    private static int passengerCarMaxHeight = 2000;
    private static int controllerMaxHeight = 4000;

    private static int passengerCarPrice = 100;
    private static int cargoCarPrice = 250;
    private static int vehicleAdditionalPrice = 200;

    public static void main(String[] args)
    {
        System.out.println("Vehicles to generate: ");

        Scanner scanner = new Scanner(System.in);
        int carsCount = scanner.nextInt();

        for(int i = 0; i < carsCount; i++)
        {
            Car car = Camera.getNextCar();
            System.out.println(car);

            //let vehicle go fo free
            if (car.isSpecial) {
                openWay();
                continue;
            }

            //check mass and height of vehicle
            int price = calculatePrice(car);
            if(price == -1) {
                continue;
            }

            System.out.println("Total sum: " + price + " usd.");
        }
    }

    /**
     * calculate price
     */
    private static int calculatePrice(Car car)
    {
        int carHeight = car.height;
        int price = 0;
        if (carHeight > controllerMaxHeight)
        {
            blockWay("Your vehicle height is to high");
            return -1;
        }
        else if (carHeight > passengerCarMaxHeight)
        {
            double weight = car.weight;

            if (weight > passengerCarMaxWeight)
            {
                price = passengerCarPrice;
                if (car.hasVehicle) {
                    price = price + vehicleAdditionalPrice;
                }
            }

            else {
                price = cargoCarPrice;
            }
        }
        else {
            price = passengerCarPrice;
        }
        return price;
    }

    private static void openWay()
    {
        System.out.println("Have a good road");
    }

    private static void blockWay(String reason)
    {
        System.out.println("Way is blocked " + reason);
    }
}