import core.Line;
import core.Station;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RouteCalculatorTest extends TestCase {
    List<Station> route;
    List<Station> firstRoute;
    List<Station> secondRoute;
    List<Station> thirdRoute;
    RouteCalculator calc;
    List<Station> connections;
    List<Station> connectionsTwo;
    StationIndex stationIndex;

    @Override
    protected void setUp() throws Exception {
        route = new ArrayList<>();
        firstRoute = new ArrayList<>();
        secondRoute = new ArrayList<>();
        thirdRoute = new ArrayList<>();

        connections = new ArrayList<>();
        connectionsTwo = new ArrayList<>();
        stationIndex = new StationIndex();

        Line line1 = new Line(1, "First");
        Line line2 = new Line(2, "Second");
        Line line3 = new Line(3, "Third");

        stationIndex.addLine(line1);
        stationIndex.addLine(line2);
        stationIndex.addLine(line3);

        stationIndex.addStation(new Station("Chocolate", line1));
        stationIndex.addStation(new Station("Coffee", line1));
        line1.addStation(stationIndex.getStation("Chocolate", 1));
        line1.addStation(stationIndex.getStation("Coffee", 1));

        stationIndex.addStation(new Station("Chicken", line2));
        stationIndex.addStation(new Station("Beef", line2));
        line2.addStation(stationIndex.getStation("Chicken", 2));
        line2.addStation(stationIndex.getStation("Beef", 2));

        stationIndex.addStation(new Station("Strawberry", line3));
        stationIndex.addStation(new Station("Cherry", line3));
        line3.addStation(stationIndex.getStation("Strawberry", 3));
        line3.addStation(stationIndex.getStation("Cherry", 3));

        connections.add(stationIndex.getStation("Coffee", 1));
        connections.add(stationIndex.getStation("Beef", 2));

        connectionsTwo.add(stationIndex.getStation("Beef", 2));
        connectionsTwo.add(stationIndex.getStation("Strawberry", 3));

        stationIndex.addConnection(connections);
        stationIndex.addConnection(connectionsTwo);

        firstRoute.add(stationIndex.getStation("Chocolate", 1));
        firstRoute.add(stationIndex.getStation("Coffee", 1));

        secondRoute.add(stationIndex.getStation("Chocolate", 1));
        secondRoute.add(stationIndex.getStation("Coffee", 1));
        secondRoute.add(stationIndex.getStation("Beef", 2));

        thirdRoute.add(stationIndex.getStation("Chocolate", 1));
        thirdRoute.add(stationIndex.getStation("Coffee", 1));
        thirdRoute.add(stationIndex.getStation("Beef", 2));
        thirdRoute.add(stationIndex.getStation("Strawberry", 3));
        thirdRoute.add(stationIndex.getStation("Cherry", 3));

        calc = new RouteCalculator(stationIndex);

        super.setUp();
    }

    public void testShortestRouteOnOneLineWithoutConnection() {
        List<Station> actual = calc.getShortestRoute(stationIndex.getStation("Chocolate", 1),
                stationIndex.getStation("Coffee", 1));
        assertEquals(firstRoute, actual);

        List<Station> reversedActual = calc.getShortestRoute(stationIndex.getStation("Coffee", 1),
                stationIndex.getStation("Chocolate", 1));
        Collections.reverse(firstRoute);
        assertEquals(firstRoute, reversedActual);

    }


    public void testShortestRouteWithOneConnection() {
        List<Station> actual = calc.getShortestRoute(stationIndex.getStation("Chocolate", 1),
                stationIndex.getStation("Beef", 2));
        assertEquals(secondRoute, actual);
    }

    public void testShortestRouteWithTwoConnections() {
        List<Station> actual = calc.getShortestRoute(stationIndex.getStation("Chocolate", 1),
                stationIndex.getStation("Cherry", 3));
        assertEquals(thirdRoute, actual);
    }

    public void testCalculateDuration() {
        double actual = RouteCalculator.calculateDuration(firstRoute);
        double expected = 2.5;
        assertEquals(expected, actual);

        assertEquals(actual , expected);
    }
}