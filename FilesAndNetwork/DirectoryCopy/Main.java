import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, DirectoryNotEmptyException {

        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Write path to copying directory");
            String from = scanner.nextLine();
            System.out.println("Where you want to copy data ");
            String to = scanner.nextLine();

            File dst = new File(to);

            FileUtils.copyFolder(from, to);

            File[] files = dst.listFiles();
            System.out.println(Arrays.toString(files));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}