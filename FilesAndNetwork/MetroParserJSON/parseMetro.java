import core.Connections;
import core.Line;
import core.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class parseMetro {

    private final String webUrl = "https://www.moscowmap.ru/metro.html#lines";

    private Document doc;
    private List<Line> metroLines;

    parseMetro(String url) throws IOException {
        doc = Jsoup.connect(webUrl).userAgent("Mozilla/5.0 Chrome/26.0.1410.64 Safari/537.31").maxBodySize(0).get();
        metroLines = new ArrayList<>();
    }

    public JSONArray parseMetroLines() throws IOException {

        doc = Jsoup.connect(webUrl).maxBodySize(0).get();

        Line line;
        JSONArray linesArray = new JSONArray();

        for (Element el : doc.select("span.js-metro-line")) { // lines
            String number = el.attr("data-line"); // lines numbers

            line = new Line(el.ownText(), number);
            linesArray.add(line.getLine());
            metroLines.add(line);
        }
        return linesArray;
    }

    public JSONObject parseMetroStation() throws IOException {
        doc = Jsoup.connect(webUrl).maxBodySize(0).get();

        JSONObject station = new JSONObject();

        for (Element element : doc.select("div.js-metro-stations")) {
            String lineNumber = element.attr("data-line");
            JSONArray stationsArray = new JSONArray();

            for (Element stationDiv : element.select("span[class=name]")) {
                String stationName = stationDiv.text();
                System.out.println("Line number " + lineNumber + " , " + "station : " + stationName);
                stationsArray.add(stationName);
                station.put(lineNumber, stationsArray);
            }
        }
        return station;
    }

    public void parseConnections() throws IOException {
        doc = Jsoup.connect(webUrl).maxBodySize(0).get();

        for (Element element : doc.select("div.js-metro-stations")){
            for (Element connections : element.select("a:has(span[title])")){
                String station = connections.text();

                Connections stationsConnection = new Connections();
                stationsConnection.addStation(new Station(station, element.attr("data-line")));


            }
        }
    }
}