package core;

import java.util.TreeSet;

public class Connections {

    private final TreeSet<Station> stationsConnections;

    public Connections() {
        stationsConnections = new TreeSet<>();
    }

    public void addStation(Station station) {
        stationsConnections.add(station);
    }

    public TreeSet<Station> getStationsCon() {
        return stationsConnections;
    }
}